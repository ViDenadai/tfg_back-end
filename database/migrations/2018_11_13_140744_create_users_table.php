<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            // id: inteiro, auto incrementável e não sinalizado(Chave primária)
            // Identifica o número do log
            $table->increments('id');

            // user_name: varchar de tamanho 200
            // Armazena o nome do usuário
            $table->string('user_name', 200)->unique();

            // email: varchar de tamanho 250
            // Armazena o email do usuário
            $table->string('email', 250);

            // password: varchar de tamanho 200
            // Armazena a senha do usuário
            $table->string('password', 200);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
