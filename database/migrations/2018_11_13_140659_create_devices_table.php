<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('devices', function (Blueprint $table) {
            // device_name: varchar de tamanho 350 (Chave primária)
            // Armazena nome do dispositivo
            $table->string('device_name', 350);
            $table->primary('device_name');

            // ip: varchar de tamanho 350
            // Armazena o endereço para comunicação com o dispositivo
            $table->string('ip', 350);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('devices');
    }
}
