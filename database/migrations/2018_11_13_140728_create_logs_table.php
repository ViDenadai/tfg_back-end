<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs', function (Blueprint $table) {
            // id: inteiro, auto incrementável e não sinalizado(Chave primária)
            // Identifica o número do log
            $table->increments('id');

            // message: varchar de tamanho 350
            // Contém o que ocorreu com o componente relacionado ao log
            $table->string('message', 350);

            // before_state: varchar de tamanho 200
            // Contém o valor estado anterior à mudança do componente
            $table->string('before_state', 200)->nullable()->default(null);

            // current_state: varchar de tamanho 200
            // Contém o valor do estado após a mudança do componente
            $table->string('current_state', 200)->nullable()->default(null);

            // created_at: datetime com valor padrão igual ao
            // dia e hora de criação
            $table->dateTime('created_at')->useCurrent();

            // component_id: inteiro (Chave estrangeira)
            // Relaciona o log ao seu componente
            $table->integer('component_id')->unsigned();
            $table->foreign('component_id')
                ->references('id')
                ->on('components')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logs');
    }
}
