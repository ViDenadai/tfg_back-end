<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComponentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('components', function (Blueprint $table) {
            // id: inteiro, auto incrementável e não sinalizado(Chave primária)
            // Identifica o número do componente
            $table->increments('id');

            // component_name: varchar de tamanho 100
            // Armazerna o nome do componente
            $table->string('component_name', 100);

            // state: varchar de tamanho 200 com valor inicial nulo
            // Armazena o estado atual ou passagens de estado do componente
            $table->string('state', 200)->nullable()->default(null);

            // device_name: varchar de tamanho 350 (Chave estrangeira)
            // Relaciona o componentes ao seu dispositivo
            $table->string('device_name', 350);
            $table->foreign('device_name')
                ->references('device_name')
                ->on('devices')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('components');
    }
}
