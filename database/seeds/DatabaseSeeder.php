<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(DeviceTableSeeder::class);
        $this->call(ComponentTableSeeder::class);
        $this->call(LogTableSeeder::class);
        $this->call(UserTableSeeder::class);
    }
}
