<?php

use Illuminate\Database\Seeder;

class ComponentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('components')->insert([
            'component_name' => 'led_1',
            'state' => '1',
            'device_name' => 'device_0',
        ]);
        DB::table('components')->insert([
            'component_name' => 'led_2',
            'state' => '1',
            'device_name' => 'device_0',
        ]);
        DB::table('components')->insert([
            'component_name' => 'pot_01',
            'state' => '500',
            'device_name' => 'device_0',
        ]);
    }
}
