<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class LogTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('logs')->insert([
            'message' => 'Dispositivo criado: device_0; Componente criado: led_1; Estado atual: 1;',
            'before_state' => null,
            'current_state' => '1',
            'created_at' => Carbon::now(),
            'component_id' => 1,
        ]);

        DB::table('logs')->insert([
            'message' => 'Dispositivo atualizado: device_0; Componente atualizado: led_1; Estado anterior: 1; Estado atual: 0;',
            'before_state' => '1',
            'current_state' => '0',
            'created_at' => Carbon::now()->addMinute(),
            'component_id' => 1,
        ]);
    }
}
