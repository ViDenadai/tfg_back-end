<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    // Deixa explícito quais campos da model podem ser alterados quando
    // um vetor é passado à Model
    protected $fillable = [
        'name', 'ip_port', 'is_connected'
    ];

    // Relação de 1:N entre Device e Component
    public function components()
    {
        return $this->hasMany('App\Component');
    }
}
