<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * Login do usuário
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $credentials = request(['user_name', 'password']);

        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Não autorizado...'], 401);
        }

        return $this->respondWithToken($token);
    }

    /**
     * Logout do usuário
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Logout feito com sucesso.']);
    }

    /**
     * Verificação do token
     *
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function verifyToken(Request $request)
    {
        if (!is_null($request->header('Authorization'))) {
            if(auth()->parseToken()->check()) {
                // É devolvido apenas o token do Header "Authorization"
                // return response()->json(['auth' => substr($request->header('Authorization') , 7)]);
                return response()->json(['auth' => true]);
            } 
        }
        
        return response()->json(['auth' => false]);
    }

    /**
     * Estruturação do array de retorno com o token e o tempo de expiração
     *
     * @param String $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {    
        return response()->json([
            'access_token' => $token,
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }
}
