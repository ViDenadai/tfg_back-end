<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Método que retorna todos os usuários existentes.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $users = User::all();
        return response()->json($users);
    }

    /**
     * Método que retorna o usuário especificado pela chave primária.
     *
     * @param integer $id
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $user = User::find($id);

        if(!$user) {
            return response()->json([
                'message'   => 'Usuário não encontrado...',
            ], 404);
        }

        return response()->json($user);
    }

    /**
     * Método que insere um usuário.
     *
     * @param \Illuminate\Http\Request $request
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $user = new User();
        $user->fill($request->all());
        $user->save();

        return response()->json($user, 201);
    }

    /**
     * Método que atualiza um usuário.
     *
     * @param \Illuminate\Http\Request $request
     * @param integer $id
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);

        if(!$user) {
            return response()->json([
                'message'   => 'Usuário não encontrado...',
            ], 404);
        }

        $user->fill($request->all());
        $user->save();

        return response()->json($user);
    }

    /**
     * Método que remove um usuário.
     *
     * @param integer $id
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $user = User::find($id);

        if(!$user) {
            return response()->json([
                'message'   => 'Usuário não encontrado...',
            ], 404);
        }

        $user->delete();
    }
}
