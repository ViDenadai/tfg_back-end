<?php

namespace App\Http\Controllers;

use App\Log;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LogController extends Controller
{
    /**
     * Método que retorna todos os logs existentes.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $logs = Log::with('component')->get();
        return response()->json($logs);
    }

    /**
     * Método que retorna o log especificado pela chave primária.
     *
     * @param integer $id
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $log = Log::find($id);

        if(!$log) {
            return response()->json([
                'message'   => 'Log não encontrado...',
            ], 404);
        }

        return response()->json($log);
    }

    /**
     * Método que insere um log.
     *
     * @param \Illuminate\Http\Request $request
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $log = new Log();
        $log->fill($request->all());
        $log->save();

        return response()->json($log, 201);
    }

    /**
     * Método que atualiza um log.
     *
     * @param \Illuminate\Http\Request $request
     * @param integer $id
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $log = Log::find($id);

        if(!$log) {
            return response()->json([
                'message'   => 'Log não encontrado...',
            ], 404);
        }

        $log->fill($request->all());
        $log->save();

        return response()->json($log);
    }

    /**
     * Método que remove um log.
     *
     * @param integer $id
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $log = Log::find($id);

        if(!$log) {
            return response()->json([
                'message'   => 'Log não encontrado...',
            ], 404);
        }

        $log->delete();
    }
}
