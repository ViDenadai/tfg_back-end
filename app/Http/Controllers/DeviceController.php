<?php

namespace app\Http\Controllers;

use App\Device;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DeviceController extends Controller
{
    /**
     * Método que retorna todos os dispositivos existentes.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $devices = Device::all();
        return response()->json($devices);
    }

    /**
     * Método que retorna o dispositivo especificado pela chave primária.
     *
     * @param String $device_name
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($device_name)
    {
        $device = Device::find($device_name);

        if(!$device) {
            return response()->json([
                'message'   => 'Dispositivo não encontrado...',
            ], 404);
        }

        return response()->json($device);
    }

    /**
     * Método que insere um dispositivo.
     *
     * @param String $device_name
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $device = new Device();
        $device->fill($request->all());
        $device->save();

        return response()->json($device, 201);
    }

    /**
     * Método que atualiza um dispositivo.
     *
     * @param \Illuminate\Http\Request $request
     * @param String $device_name
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $device_name)
    {
        $device = Device::find($device_name);

        if(!$device) {
            return response()->json([
                'message'   => 'Dispositivo não encontrado...',
            ], 404);
        }

        $device->fill($request->all());
        $device->save();

        return response()->json($device);
    }

    /**
     * Método que remove um dispositivo.
     *
     * @param String $device_name
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($device_name)
    {
        $device = Device::find($device_name);

        if(!$device) {
            return response()->json([
                'message'   => 'Dispositivo não encontrado...',
            ], 404);
        }

        $device->delete();
    }
}
