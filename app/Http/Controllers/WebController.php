<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WebController extends Controller
{
    /**
     * Método para recuperar todos os devices com os seus respectivos componentes e seus valores
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public static function getAllDevCompInformation()
    {
        $results = DB::table('devices')                    
                    ->select('devices.device_name', 'components.component_name', 'components.state')
                    ->join('components', 'devices.device_name', '=', 'components.device_name')                    
                    ->get();
        $results_formated = [];
        if (!$results->isEmpty()) {
            foreach ($results as $data) {
                $aux = [];
                if (!isset($results_formated[$data->device_name])) {
                    $results_formated[$data->device_name] = [];
                }
                $aux['nome'] = $data->component_name;
                $aux['estado'] = $data->state;
                array_push($results_formated[$data->device_name], $aux);
            }
        }                    
        return response()->json(['data' => $results_formated]);
    }

    /**
     * Método para recuperar todos os logs de um device
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public static function getAllLogs(Request $request)
    {

        if (!empty($request->input('device_name'))) {
            $results = DB::table('logs')                    
                    ->select('logs.message', 'logs.created_at')            
                    ->join('components', 'logs.component_id', '=', 'components.id')                    
                    ->where('components.device_name', '=', $request->input('device_name'))
                    ->get();
        } else {
            return response()->json(['error' => 'Requisição inválida'], 400);
        }
        
        if (!$results->isEmpty()) {
            $results_formated = [];
            foreach ($results as $data) {
                $aux = [];
                $aux['registro'] = $data->message;
                $aux['data'] = date("d/m/Y h:i:s", strtotime($data->created_at));
                array_push($results_formated, $aux);
            }
            return response()->json(['data' => $results_formated]);
        } else {
            return response()->json(['data' => '']);
        }
    }
}
