<?php

namespace App\Http\Controllers;

use App\Component;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ComponentController extends Controller
{
    /**
     * Método que retorna todos os componentes existentes.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $component = Component::with('device')->get();
        return response()->json($component);
    }

    /**
     * Método que retorna o componente especificado pela chave primária.
     *
     * @param integer $id
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $component = Component::find($id);

        if(!$component) {
            return response()->json([
                'message'   => 'Componente não encontrado...',
            ], 404);
        }

        return response()->json($component);
    }

    /**
     * Método que insere um componente.
     *
     * @param \Illuminate\Http\Request $request
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $component = new Component();
        $component->fill($request->all());
        $component->save();

        return response()->json($component, 201);
    }

    /**
     * Método que atualiza um componente.
     *
     * @param \Illuminate\Http\Request $request
     * @param integer $id
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $component = Component::find($id);

        if(!$component) {
            return response()->json([
                'message'   => 'Componente não encontrado...',
            ], 404);
        }

        $component->fill($request->all());
        $component->save();

        return response()->json($component);
    }

    /**
     * Método que remove um componente.
     *
     * @param integer $id
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $component = device::find($id);

        if(!$component) {
            return response()->json([
                'message'   => 'Component não encontrado...',
            ], 404);
        }

        $component->delete();
    }
}
