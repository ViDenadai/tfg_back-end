<?php

namespace app\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HandlerController extends Controller
{
    /**
     * Método que persiste as variáveis recebidas.
     *
     * @param \Illuminate\Http\Request $request
     * 
     * @return String
     */
    public static function regVar(Request $request)
    {
        // Mensagem contendo as variáveis seguindo o protocolo
        $msg = $request->input('msg');

        // Ip do dispositivo(caso seja via TCP o ip vem pelo conteúdo da mensagem 
        // além de ter a informação de qual socket o hardware está conectado)
        $ip =  $request->has('ip') ? $request->input('ip') : $request->ip();

        // Cada componente é persistido
        $components = explode(';', $msg);        
        foreach ($components as $component) {
            // Partes da mensagem
            $parts = explode('|', $component);
            
            // Nome do dispositivo
            if (strncmp('N:', $parts[0], 2) == 0) {
                $device_name = substr($parts[0], 2);
            }
            
            // Nome do componente            
            if (strncmp('C:', $parts[1], 2) == 0) {
                $component_name = substr($parts[1], 2);
            }

            // Valor do componente            
            if (strncmp('V:', $parts[2], 2) == 0) {
                $component_value = substr($parts[2], 2);
            }

            if (isset($device_name) && isset($component_name) && isset($component_value)) {
                // Mensagem de log
                $log_message = '';
                
                // Se o dispositivo existe no banco o ip do device 
                // é atualizado e em caso negativo um novo registro é criado
                $db_device_name = DB::table('devices')
                                    ->where('device_name', $device_name)
                                    ->first();
                if (!is_null($db_device_name)) {
                    DB::table('devices')
                        ->where('device_name', $device_name)
                        ->update(['ip' => $ip]);
                    $log_message = "Dispositivo atualizado: " . $device_name . "; ";
                } else {
                    DB::table('devices')
                        ->insert(
                            ['device_name' => $device_name, 'ip' => $ip]);
                    $log_message = "Dispositivo criado: " . $device_name . "; ";
                }

                // Se o componente do dispositivo existe no banco o estado 
                // do componente é atualizado e em caso negativo um novo 
                // registro é criado
                $db_component = DB::table('components')
                                    ->join('devices', 'components.device_name', '=', 'devices.device_name')
                                    ->where('devices.device_name', $device_name)
                                    ->where('components.component_name', $component_name)
                                    ->first();
                
                if (!is_null($db_component)) {
                    $component_id = $db_component->id;
                    $before_state = $db_component->state;
                    DB::table('components')
                        ->where('id', $db_component->id)
                        ->update(['state' => $component_value]);
                    $log_message .= "Componente atualizado: " . $component_name . "; Estado anterior: " . $before_state . "; Estado atual: " . $component_value . ";";
                } else {
                    $component_id = DB::table('components')
                                        ->insertGetId(
                                            ['component_name' => $component_name, 'state' => $component_value, 'device_name' => $device_name]);
                    $log_message .= "Componente criado: " . $component_name . "; Estado atual: " . $component_value . ";";
                }

                // Inserção do log (Se é um novo componente ele não possui before_state)
                if (isset($before_state)) {
                    DB::table('logs')
                        ->insert(
                            ['message' => $log_message, 'before_state' => $before_state, 'current_state' => $component_value, 'component_id' => $component_id]);
                } else {
                    DB::table('logs')
                        ->insert(
                            ['message' => $log_message, 'before_state' => null, 'current_state' => $component_value, 'component_id' => $component_id]);
                }
            }
        }
                
        return 'Tudo certo!!!';
    }    
}