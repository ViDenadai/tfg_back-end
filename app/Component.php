<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Component extends Model
{
    // Deixa explícito quais campos da model podem ser alterados quando
    // um vetor é passado à Model
    protected $fillable = [
        'name', 'state', 'device_id'
    ];

    // Relação de N:1 entre Component e Device 
    public function device()
    {
        return $this->belongsTo('App\Device');
    }

    // Relação de 1:N entre Component e Log
    public function logs()
    {
        return $this->hasMany('App\Log');
    }
}
