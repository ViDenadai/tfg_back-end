<?php
require_once("SocketServer.php");

// Desabilita o php notice
error_reporting(0); 

// Inicia o servidor no ip e porta especificados (No caso ip é "localhost" e porta é 31337)
$server = new SocketServer("localhost", 31337); 

// Quantidade máxima de clientes
$server->max_clients = 10;  

// Função que executa quando o cliente envia algum dado
$server->hook("INPUT","handle_input"); 

// O código é executado infinitamente até o processo ser finalizado
$server->infinite_loop();

// Função para quando o cliente envia algum dado
function handle_input(&$server,&$client,$input)
{
    // Remoção dos marcadores de final de linha e espaços em branco
    $trim = trim($input);

    // Desconecta o cliente em caso de mensagem "quit"    
    if(strtolower($trim) == "quit")
    {
        // Desconecta o cliente
        $server->disconnect($client->server_clients_index); 
        return;
    }

    // Conteúdo do POST
    $postdata = http_build_query(
        array(
            'msg' => $trim,
            'ip' => $client->ip . ' ' . $client->socket
        )
    );

    // Opções da requisição
    $opts = array('http' =>
        array(
            'method'  => 'POST',
            'header'  => 'Content-Type: application/x-www-form-urlencoded',
            'content' => $postdata
        )
    );

    // Execução da requisição ao servidor
    $context  = stream_context_create($opts);
    $output = file_get_contents("http://localhost:8000/api/server/regVar", false, $context);    
}