<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * Deixa explícito quais campos relacionados à model podem ser alterados quando
     * um vetor é passado para ser persistido no banco de dados
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * Deixa explícito quais campos da model são escondidos quando um vetor 
     * é retornado do banco de dados
     * 
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * Retorna o identificador para adicionar ao token JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Retorna um array customizado para adicionar ao token JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
