<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    // Deixa explícito quais campos da model podem ser alterados quando
    // um vetor é passado à Model
    protected $fillable = [
        'message'
    ];

    // Campos de datas
    protected $dates = [
        'created_at'
    ];

    // Relação de N:1 entre Log e Component 
    public function component()
    {
        return $this->belongsTo('App\Component');
    }
}
