<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(array('prefix' => 'server'), function()
{
    Route::get('/', function () 
    {
        return 'Conexão Estabelecida...';
    });

    // Rotas protegidas pelo 'jwt.auth'
    Route::group(['middleware' => ['jwt.auth']], function () {
        Route::resource('devices', 'DeviceController');
        Route::resource('components', 'ComponentController');
        Route::resource('logs', 'LogController');
        Route::resource('users', 'UserController');
        Route::delete('logout', 'AuthController@logout');
        Route::get('getAllDevCompInformation', 'WebController@getAllDevCompInformation');    
        Route::get('getAllLogs', 'WebController@getAllLogs');                
    });

    Route::post('login', 'AuthController@login');
    Route::post('verifyToken', 'AuthController@verifyToken');
    Route::name('handler.')->group(function () {
        Route::post('/regVar', ['as' => 'regVar', 'uses' => 'HandlerController@regVar']);
    });
});

Route::get('/', function () {
    return redirect('server');
});